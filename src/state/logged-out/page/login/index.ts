import { Next } from "futura";

import * as authentication from "app/services/storage/";

/** State */
export class LoginPage implements LoginPage.State {
  public static init = (loginError?: string): Next<LoginPage> => ({
    state: new LoginPage(
      {
        username: "",
        password: "",
      },
      LoginPage.Status.Pending,
      loginError,
    ),
  })

  public update(message: any): Next<LoginPage> {
    if (message instanceof UpdateUsername
      && this.status !== LoginPage.Status.LoggingIn) {
      return {
        state: new LoginPage(
          { ...this.credentials, username: message.username },
          LoginPage.Status.Pending,
        ),
      };
    } else if (message instanceof UpdatePassword
        && this.status !== LoginPage.Status.LoggingIn) {
      return {
        state: new LoginPage(
          {  ...this.credentials, password: message.password },
          LoginPage.Status.Pending,
        ),
      };
    } else if (message instanceof UpdatePassword
        && this.status !== LoginPage.Status.LoggingIn) {
      return {
        state: new LoginPage(
          {  ...this.credentials, password: message.password },
          LoginPage.Status.Pending,
        ),
      };
    } else if (message instanceof Login) {
      const { username } = this.credentials;

      return {
        state: new LoginPage(
          this.credentials,
          LoginPage.Status.LoggingIn,
        ),
        requests: [
          authentication.signIn(
            username,
            LoginOk,
            LoginError,
            {
              onSuccess: message.onSuccess,
            },
          ),
        ],
      };
    } else if (message instanceof LoginOk
        && this.status === LoginPage.Status.LoggingIn) {
        if (message.context.onSuccess) {
          message.context.onSuccess(message.profile);
        }

        return {
          state: new LoginPage(
            this.credentials,
            LoginPage.Status.LoggedIn,
          ),
        };
    } else if (message instanceof LoginError
        && this.status === LoginPage.Status.LoggingIn) {
      return {
        state: new LoginPage(
          this.credentials,
          LoginPage.Status.Error,
          message.info.message,
        ),
      };
    } else {
      return { state: this };
    }
  }

  public subscriptions = () => [
  ]

  private constructor(
    readonly credentials: LoginPage.Credentials,
    readonly status: LoginPage.Status,
    readonly error?: string,
  ) {}
}

/** Types */

export namespace LoginPage {
  export interface State {
    readonly credentials: LoginPage.Credentials;
    readonly status: Status;
    readonly error?: string;
  }

  export interface Credentials {
    readonly username: string;
    readonly password: string;
  }

  export const enum Status {
    Pending,
    LoggingIn,
    LoggedIn,
    Error,
  }

  export type Message
    = UpdateUsername
    | UpdatePassword
    | Login
    | LoginOk
    | LoginError;
}


/** Messages */

export class UpdateUsername {
  constructor(
    readonly username: string,
  ) {}
}

export class UpdatePassword {
  constructor(
    readonly password: string,
  ) {}
}

export class Login {
  constructor(
    readonly onSuccess: (profile: authentication.User.Profile) => void,
  ) {}
}

class LoginOk {
  constructor(
    readonly profile: authentication.User.Profile,
    readonly context: Context,
  ) {}
}

class LoginError {
  constructor(
    readonly info: ErrorInfo,
    readonly context: Context,
  ) {}
}

interface Context {
  readonly onSuccess: (profile: authentication.User.Profile) => void;
}

export interface ErrorInfo {
  code: number;
  message: string;
  data: any;
}

export namespace ErrorInfo {
  export const make = (info: Partial<ErrorInfo>) => ({
    code: info.code || 0,
    message: info.message || "Oops, something went wrong",
    data: info.data || {},
  });
}
