import { Next } from "futura";

import * as authentication from "app/services/storage/";

/** State */
export class RegistrationPage implements RegistrationPage.State {
  public static init = (): Next<RegistrationPage> => ({
    state: new RegistrationPage(
      { email: "", password: "" },
      "",
      RegistrationPage.Status.Pending,
    ),
    requests: [],
  })

  public update(message: any): Next<RegistrationPage> {
    if (message instanceof UpdateEmail
      && this.status !== RegistrationPage.Status.Registering) {
      return {
        state: new RegistrationPage(
          { ...this.credentials, email: message.email},
          this.confirmPassword,
          RegistrationPage.Status.Pending,
        ),
      };
    } else if (message instanceof UpdatePassword
        && this.status !== RegistrationPage.Status.Registering) {

      return {
        state: new RegistrationPage(
          {  ...this.credentials, password: message.password },
          this.confirmPassword,
          RegistrationPage.Status.Pending,
        ),
      };
    } else if (message instanceof UpdateConfirmPassword
      && this.status !== RegistrationPage.Status.Registering) {
      return {
        state: new RegistrationPage(
          this.credentials,
          message.confirmPassword,
          RegistrationPage.Status.Pending,
        ),
      };
    } else if (message instanceof Register) {
      const { email, password } = this.credentials;

      return password === this.confirmPassword
      ? {
          state: new RegistrationPage(
            this.credentials,
            this.confirmPassword,
            RegistrationPage.Status.Registering,
          ),
          requests: [
            authentication.register(
              email,
              RegisterOk,
              RegisterError,
              {
                onSuccess: message.onSuccess,
              },
            ),
          ],
        }
      : {
        state: new RegistrationPage(
          this.credentials,
          "",
          RegistrationPage.Status.Error,
          "Passwords Do Not Match",
        ),
      };
    } else if (message instanceof RegisterOk
        && this.status === RegistrationPage.Status.Registering) {
      if (message.context.onSuccess) {
        alert("User Registered");
        message.context.onSuccess();
      }

      return {
        state: new RegistrationPage(
          this.credentials,
          this.confirmPassword,
          RegistrationPage.Status.Registered,
        ),
      };
    } else if (message instanceof RegisterError
        && this.status === RegistrationPage.Status.Registering) {
      return {
        state: new RegistrationPage(
          this.credentials,
          this.confirmPassword,
          RegistrationPage.Status.Error,
          message.info.message,
        ),
      };
    } else {
      return { state: this };
    }
  }

  public subscriptions = () => [
  ]

  private constructor(
    readonly credentials: RegistrationPage.Credentials,
    readonly confirmPassword: string,
    readonly status: RegistrationPage.Status,
    readonly error?: string,
  ) {}
}


/** Types */

export namespace RegistrationPage {
  export interface State {
    readonly credentials: Credentials;
    readonly confirmPassword: string;
    readonly status: Status;
    readonly error?: string;
  }

  export interface Credentials {
    readonly email: string;
    readonly password: string;
  }

  export const enum Status {
    Pending,
    Registering,
    Registered,
    Error,
  }

  export type Message
    = UpdateEmail
    | UpdatePassword
    | UpdateConfirmPassword
    | Register
    | RegisterOk
    | RegisterError;
}


/** Messages */

export class UpdateEmail {
  constructor(
    readonly email: string,
  ) {}
}

export class UpdatePassword {
  constructor(
    readonly password: string,
  ) {}
}

export class UpdateConfirmPassword {
  constructor(
    readonly confirmPassword: string,
  ) {}
}

export class Register {
  constructor(
    readonly onSuccess?: () => void,
  ) {}
}

class RegisterOk {
  constructor(
    readonly email: string,
    readonly context: Context,
  ) {}
}

class RegisterError {
  constructor(
    readonly info: ErrorInfo,
    readonly context: Context,
  ) {}
}

interface Context {
  readonly onSuccess?: () => void;
}

export interface ErrorInfo {
  code: number;
  message: string;
  data: any;
}

export namespace ErrorInfo {
  export const make = (info: Partial<ErrorInfo>) => ({
    code: info.code || 0,
    message: info.message || "Oops, something went wrong",
    data: info.data || {},
  });
}
