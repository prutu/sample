import { Next, Req, Sub } from "futura";

import { LoggedIn } from "app/state/logged-in";

import { User } from "app/services/storage/model";
import { LoginPage } from "./page/login";
import { RegistrationPage } from "./page/register";


/** State */

export class LoggedOut implements LoggedOut.State {
  public static init = (loginError?: string): Next<LoggedOut> => {
    const instance = Symbol();
    const page = LoginPage.init(loginError);

    return {
      state: new LoggedOut(
        instance,
        [ page.state ],
      ),
      requests: page.requests || [],
    };
  }

  public update(message: any): Next<LoggedOut | LoggedIn> {
    if (message instanceof Exit && message.instance === this.instance) {
      return LoggedOut.init();
    } else if (message instanceof PushPage && message.instance === this.instance) {
      const page = Pageinit(message.page);

      return {
        state: new LoggedOut(
          this.instance,
          [ ...this.pages, page.state ],
        ),
        requests: page.requests,
      };
    } else if (message instanceof Back && message.instance === this.instance && this.pages.length > 1) {
      return {
        state: new LoggedOut(
          this.instance,
          this.pages.slice(0, -1),
        ),
      };
    } else if (message instanceof Login && message.instance === this.instance) {
      return LoggedIn.init(message.user);
    } else {
      const pages = this.pages.map((page) => page.update(message));

      return {
        state: new LoggedOut(
          this.instance,
          pages.map((page) => page.state),
        ),
        requests: pages.reduce((acc, page) =>
          [...acc, ...(page.requests || [])], [] as Req[]),
      };
    }
  }

  public subscriptions = () =>
    this.pages.reduce((acc, page) =>
      [...acc, ...page.subscriptions()], [] as Sub[])

  private constructor(
    readonly instance: symbol,
    readonly pages: ReadonlyArray<LoggedOut.Page>,
  ) {}
}


export const Pageinit = (page: LoggedOut.PageRequest): Next<LoggedOut.Page> => {
    switch (page.type) {
      case LoggedOut.Page.Type.Login:
        return LoginPage.init();
      case LoggedOut.Page.Type.Register:
        return RegistrationPage.init();
  }
};


/** Types */

export namespace LoggedOut {
  export interface State {
    readonly instance: symbol;
    readonly pages: ReadonlyArray<Page>;
  }

  export type Page
    = LoginPage
    | RegistrationPage;

  export namespace Page {
    export enum Type {
      Login,
      Register,
    }
  }

  export type PageRequest
  = { type: Page.Type.Login }
  | { type: Page.Type.Register };

  export type Message
    = Login
    | Exit
    | Back
    | PushPage
    | LoginPage.Message
    | RegistrationPage.Message;
}


/** Messages */

export class Login {
  constructor(
    readonly user: User.Profile,
    readonly instance: symbol,
  ) {}
}

export class Exit {
  constructor(
    readonly instance: symbol,
  ) {}
}

export class Back {
  constructor(
    readonly instance: symbol,
  ) {}
}

export class PushPage {
  constructor(
    readonly page: LoggedOut.PageRequest,
    readonly instance: symbol,
  ) {}
}
