import { Next } from "futura";

import { Loading } from "./loading";
import { LoggedIn } from "./logged-in";
import { LoggedOut } from "./logged-out";


/** Initialize the application state */
export const init: () => Next<State> = Loading.init;

/** Update the application state given the current state and a message */
export const update = (state: State, message: Message): Next<State> =>
  state.update(message);

/** Return the list of necessary subscriptions for the current state */
export const subscriptions = (state: State) =>
  state.subscriptions();


/** Types */

/** Possible states for the application */

export type State
  = Loading
  | LoggedOut
  | LoggedIn;

/** Possible messages to be handled by the various states */
export type Message
  = Loading.Message
  | LoggedOut.Message
  | LoggedIn.Message;
