import { User } from "app/services/storage/model";

import { Next } from "futura";


/** State */

export class HomeTab implements HomeTab.State {
  public static init = (user: User.Profile): Next<HomeTab> => ({
    state: new HomeTab(
      user,
    ),
  })

  public update(message: any): Next<HomeTab> {
    if (message instanceof HomeTabMessage) {
      return {
        state: new HomeTab(
          this.user,
        ),
      };
    } else {
      return { state: this };
    }
  }

  public subscriptions = () => [
  ]

  private constructor(
    readonly user: User.Profile,
  ) {}
}


/** Types */

export namespace HomeTab {
  export interface State {
    readonly user: User.Profile;
  }

  export type Message
    = HomeTabMessage;
}


/** Messages */

class HomeTabMessage {
}
