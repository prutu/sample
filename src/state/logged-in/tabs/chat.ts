import { Next } from "futura";

import { User } from "app/services/storage";


/** State */

export class ChatTab implements ChatTab.State {
  public static init = (user: User.Profile): Next<ChatTab> => ({
    state: new ChatTab(user),
  })

  public update(message: any): Next<ChatTab> {
    if (message instanceof ChatTabMessage) {
      return {
        state: new ChatTab(
          this.user,
        ),
      };
    } else {
      return { state: this };
    }
  }

  public subscriptions = () => [
  ]

  private constructor(
    readonly user: User.Profile,
  ) {}
}


/** Types */

export namespace ChatTab {
  export interface State {
    readonly user: User.Profile;
  }

  export type Message
    = ChatTabMessage;
}


/** Messages */

class ChatTabMessage {}
