import { User } from "app/services/storage/";

import { signOut } from "app/services/storage";
import { Next } from "futura";

/** State */

export class SettingsTab implements SettingsTab.State {
  public static init = (user: User.Profile): Next<SettingsTab> => ({
    state: new SettingsTab(
      user,
      SettingsTab.Status.Pending,
    ),
  })

  public update(message: any): Next<SettingsTab> {
    if (message instanceof Logout &&
      this.status === SettingsTab.Status.LoggingOut) {
      return {
        state: new SettingsTab(
          this.user,
          SettingsTab.Status.LoggingOut,
        ),
        requests: [
          signOut(
            LogoutOk,
            LogoutError,
            {
              onLogout: message.onLogout,
            },
          ),
        ],
      };
    } else if (message instanceof LogoutOk
      && this.status === SettingsTab.Status.LoggingOut) {
      return {
        state: new SettingsTab(
          this.user,
          SettingsTab.Status.LoggedOut,
        ),
      };
    } else if (message instanceof LogoutError
      && this.status === SettingsTab.Status.LoggingOut) {
      return {
        state: new SettingsTab(
          this.user,
          SettingsTab.Status.Error,
          message.info.message,
        ),
      };
    } else {
      return { state: this };
    }
  }

  public subscriptions = () => [
  ]

  private constructor(
    readonly user: User.Profile,
    readonly status: SettingsTab.Status,
    readonly error?: string,
  ) {}
}


/** Types */

export namespace SettingsTab {
  export interface State {
    readonly user: User.Profile;
    readonly status: Status;
    readonly error?: string;
  }

  export const enum Status {
    Pending,
    LoggingOut,
    LoggedOut,
    Error,
  }

  export type Message
    = SettingsMessage
    | Logout
    | LogoutOk
    | LogoutError;
}


/** Messages */

export class SettingsMessage {}

export class Logout {
  constructor(
    readonly onLogout: () => void,
  ) {}
}

export class LogoutOk {
  constructor(
    readonly loggedOut: boolean,
    readonly context: Context,
  ) {}
}

export class LogoutError {
  constructor(
    readonly info: ErrorInfo,
  ) {}
}

interface Context {
  readonly onLogout: () => void;
}

export interface ErrorInfo {
  code: number;
  message: string;
  data: any;
}

export namespace ErrorInfo {
  export const make = (info: Partial<ErrorInfo>) => ({
    code: info.code || 0,
    message: info.message || "Oops, something went wrong",
    data: info.data || {},
  });
}
