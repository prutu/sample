import { Next } from "futura";

import { ChatTab } from "./tabs/chat";
import { HomeTab } from "./tabs/home";
import { SettingsTab } from "./tabs/settings";

import { User } from "app/services/storage/model";
import { LoggedOut } from "../logged-out";

/** State */

export class LoggedIn implements LoggedIn.State {
  public static init = (user: User.Profile): Next<LoggedIn> => {
    const instance = Symbol();
    const settings = SettingsTab.init(user);
    const home = HomeTab.init(user);
    const chat = ChatTab.init(user);

    const tab = LoggedIn.Tab.HomeTab;

    return {
      state: new LoggedIn(
        user,
        instance,
        tab,
        settings.state,
        home.state,
        chat.state,
      ),
      requests: [
        ...(settings.requests || []),
        ...(home.requests || []),
        ...(chat.requests || []),
      ],
    };
  }

  public update(message: any): Next<LoggedIn | LoggedOut> {
    if (message instanceof SelectTab && message.instance === this.instance) {
      if (message.tab !== this.tab) {
        return {
          state: new LoggedIn(
            this.user,
            this.instance,
            message.tab,
            this.settings,
            this.home,
            this.chat,
          ),
        };
      } else {
        const tab = message.tab;
        const settings: Next<SettingsTab> = tab === LoggedIn.Tab.SettingsTab
          ? SettingsTab.init(this.user)
          : { state: this.settings };
        const home: Next<HomeTab> = tab === LoggedIn.Tab.HomeTab
          ? HomeTab.init(this.user)
          : { state: this.home };
        const chat: Next<ChatTab> = tab === LoggedIn.Tab.ChatTab
          ? ChatTab.init(this.user)
          : { state: this.chat };

        return {
          state: new LoggedIn(
            this.user,
            this.instance,
            tab,
            settings.state,
            home.state,
            chat.state,
          ),
          requests: [
            ...(settings.requests || []),
            ...(home.requests || []),
            ...(chat.requests || []),
          ],
        };
      }
    } else if (message instanceof Logout && message.instance === this.instance) {
      return LoggedOut.init();
    } else {
      const settings = this.settings.update(message);
      const home = this.home.update(message);
      const chat = this.chat.update(message);

      return {
        state: new LoggedIn(
          this.user,
          this.instance,
          this.tab,
          settings.state,
          home.state,
          chat.state,
        ),
        requests: [
          ...(settings.requests || []),
          ...(home.requests || []),
          ...(chat.requests || []),
        ],
      };
    }
  }

  public subscriptions = () => [
  ]

  private constructor(
    readonly user: User.Profile,
    readonly instance: symbol,
    readonly tab: LoggedIn.Tab,
    readonly settings: SettingsTab,
    readonly home: HomeTab,
    readonly chat: ChatTab,
  ) {}
}


/** Types */

export namespace LoggedIn {
  export interface State {
    readonly user: User.Profile;
    readonly instance: symbol;
    readonly tab: Tab;
    readonly settings: SettingsTab.State;
    readonly home: HomeTab.State;
    readonly chat: ChatTab.State;
  }

  export enum Tab {
    SettingsTab,
    HomeTab,
    ChatTab,
  }

  export type Message
    = SelectTab
    | Logout
    | SettingsTab.Message
    | HomeTab.Message
    | ChatTab.Message;
}


/** Messages */

export class SelectTab {
  constructor(
    public readonly tab: LoggedIn.Tab,
    public readonly instance: symbol,
  ) {}
}

export class Logout {
  constructor(
    readonly instance: symbol,
  ) {}
}
