import { Next } from "futura";

import { after } from "app/services/time";
import { LoggedOut } from "../logged-out";

/** State */

export class Loading implements Loading.State {
  public static init = (): Next<Loading> => ({
    state: new Loading(),
    requests: [
      after(500, Timeout),
    ],
  })

  public update(message: any): Next<Loading | LoggedOut> {
    if (message instanceof Timeout) {
      return LoggedOut.init();
    } else {
      return { state: this };
    }
  }

  public subscriptions = () => [
  ]

  private constructor() {}
}


/** Types */

export namespace Loading {
  export interface State {}

  export type Message
    = Timeout;
}


/** Messages */

class Timeout {
}
