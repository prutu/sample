import React from "react";

import { logo } from "app/lib/view/logo";
import { Image, StyleSheet, View } from "react-native";


export const LoadingView = () =>
  <View style={styles.container}>
    <Image
      source={logo()}
      style={styles.logotype} />
  </View>;


/** Styles */

const styles = StyleSheet.create({
  container: {
    alignItems: "center",
    backgroundColor: "white",
    flex: 1,
    justifyContent: "center",
  },
  logotype: {
    height: 100,
    width: 300,
  },
});
