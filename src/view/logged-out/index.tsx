import { StackView } from "app/lib/view/component/navigation/stack";
import { Dispatch } from "futura";
import React from "react";

import { User } from "app/services/storage/";

import { Back, Exit, LoggedOut, Login, PushPage } from "app/state/logged-out";

import { LoginPage } from "app/state/logged-out/page/login";
import { RegistrationPage } from "app/state/logged-out/page/register";

import { StatusBar, StyleSheet, Text, View } from "react-native";
import { LoginView } from "./page/login";
import { RegistrationView } from "./page/register";


export const LoggedOutView: React.SFC<Props> = ({ state, dispatch }) => {
  const onBack = back(state, dispatch);

  return (
    <View style={styles.container}>
      <StatusBar hidden={false} />
      <StackView onBack={onBack}>
        {state.pages.map((p, index) => page(p, index, state, dispatch, onBack))}
      </StackView>
    </View>
  );
};

const page = (
  page: LoggedOut.Page,
  index: number,
  state: LoggedOut.State,
  dispatch: Dispatch<LoggedOut.Message>,
  onBack: () => boolean,
) => {
  if (page instanceof LoginPage) {
    return <LoginView
              key={`login-${index}`}
              state={page}
              dispatch={dispatch}
              onRegister={viewRegistration(state.instance, dispatch)}
              onSuccess={onLogin(state.instance, dispatch)}/>;
  } else if (page instanceof RegistrationPage) {
    return <RegistrationView
              key={`register-${index}`}
              state={page}
              dispatch={dispatch}
              onSuccess={exit(state.instance, dispatch)} />;
  } else {
    return <Text>Not implemented</Text>;
  }
};


/** Messages */

const back = (state: LoggedOut.State, dispatch: Dispatch<LoggedOut.Message>) =>
  state.pages.length > 1
    ? () => (dispatch(new Back(state.instance)), true)
    : () => false;

const onLogin = (instance: symbol, dispatch: Dispatch<LoggedOut.Message>) =>
  (profile: User.Profile) =>
    dispatch(new Login(profile, instance));

const exit = (instance: symbol, dispatch: Dispatch<LoggedOut.Message>) =>
  () =>
    dispatch(new Exit(instance));

const viewRegistration = (instance: symbol, dispatch: Dispatch<LoggedOut.Message>) =>
  () =>
    dispatch(new PushPage({ type: LoggedOut.Page.Type.Register }, instance));

/** Types */

interface Props {
  readonly state: LoggedOut.State;
  readonly dispatch: Dispatch<LoggedOut.Message>;
}


/** Styles */

const styles = StyleSheet.create({
  container: {
    flex: 1,
    height: "100%",
    width: "100%",

    backgroundColor: "white",
  },
});
