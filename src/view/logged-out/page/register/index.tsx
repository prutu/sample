import { Button, Field, TextInput } from "app/lib/view/component/form";
import { LoadingView } from "app/lib/view/component/loading";

import { Dispatch } from "futura";
import React from "react";
import { StyleSheet, Text, View } from "react-native";

import {
  Register,
  RegistrationPage,
  UpdateConfirmPassword,
  UpdateEmail,
  UpdatePassword,
} from "app/state/logged-out/page/register";


export const RegistrationView: React.SFC<Props> = ({ state, dispatch, onSuccess }) => {
  const registering = state.status === RegistrationPage.Status.Registering;

  return (
    <View style={styles.container}>
      <View style={styles.form}>
        <Field
          label={"Email-Address"}>
          <TextInput
            autoCapitalize="none"
            keyboardType="email-address"
            maxLength={200}
            placeholder={"example@domain"}
            onChangeText={updateEmail(dispatch)} />
        </Field>
        <Field
          label={"Password"}>
          <TextInput
            autoCapitalize="none"
            maxLength={200}
            secureTextEntry={true}
            onChangeText={updatePassword(dispatch)} />
        </Field>
        <Field
          label={"Confirm Password"}>
          <TextInput
            autoCapitalize="none"
            maxLength={200}
            secureTextEntry={true}
            onChangeText={updateConfirmPassword(dispatch)} />
        </Field>
        <Text style={styles.error}>
          {state.status === RegistrationPage.Status.Error ? state.error : ""}
        </Text>
        <Button
          disabled={registering}
          onPress={register(onSuccess, dispatch)}
          style={styles.register}
          label={registering ? "Registering" : "Register"} />
      </View>
      {registering
      ? <LoadingView />
      : null}
    </View>
  );
};

/** Types */

interface Props {
  readonly state: RegistrationPage.State;
  readonly dispatch: Dispatch<RegistrationPage.Message>;

  readonly onSuccess: () => void;
}

/** Messages */

const updateEmail = (dispatch: Dispatch<RegistrationPage.Message>) =>
  (email: string) =>
    dispatch(new UpdateEmail(email));

const updatePassword = (dispatch: Dispatch<RegistrationPage.Message>) =>
  (password: string) =>
    dispatch(new UpdatePassword(password));

const updateConfirmPassword = (dispatch: Dispatch<RegistrationPage.Message>) =>
  (confirmPassword: string) =>
    dispatch(new UpdateConfirmPassword(confirmPassword));

const register = (onSuccess: () => void, dispatch: Dispatch<RegistrationPage.Message>) =>
() =>
  dispatch(new Register(onSuccess));

/** Styles */

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    padding: 16,
  },
  form: {
    flex: 1,
  },
  error: {
    color: "red",
    height: 20,
    marginBottom: 8,
    marginTop: 24,
  },
  field: {
    marginBottom: 16,
  },
  register: {
    marginBottom: 4,
  },
});
