import { Button, Field, TextInput } from "app/lib/view/component/form";
import { LoadingView } from "app/lib/view/component/loading";
import { Dispatch } from "futura";
import React from "react";
import { StyleSheet, Text, View } from "react-native";

import { User } from "app/services/storage/";

import {
  Login,
  LoginPage,
  UpdatePassword,
  UpdateUsername,
} from "app/state/logged-out/page/login";


export const LoginView = ({ state, dispatch, onRegister, onSuccess }: Props) => {
  const loggingIn = state.status === LoginPage.Status.LoggingIn;

  return (
    <View style={styles.container}>
      <View style={styles.form}>
        <Field
          label={"Username"}>
          <TextInput
            autoCapitalize="none"
            editable={!loggingIn}
            defaultValue={state.credentials.username}
            maxLength={200}
            onChangeText={updateUsername(dispatch)} />
        </Field>
        <Field
          label={"Password"}>
          <TextInput
            maxLength={200}
            textContentType="password"
            secureTextEntry={true}
            defaultValue={state.credentials.password}
            editable={!loggingIn}
            onChangeText={updatePassword(dispatch)} />
        </Field>
        <Text style={styles.error}>
          {state.status === LoginPage.Status.Error ? state.error : ""}
        </Text>
        <Button
          onPress={login(onSuccess, dispatch)}
          disabled={loggingIn}
          style={styles.login}
          label={loggingIn ? "Logging In" : "Log in"} />
        <Button
          onPress={onRegister}
          disabled={loggingIn}
          label={"Register"}
          style={styles.register} />
      </View>
      {loggingIn
        ? <LoadingView />
        : null}
    </View>
  );
};


/** Messages */


const updateUsername = (dispatch: Dispatch<LoginPage.Message>) =>
  (username: string) =>
    dispatch(new UpdateUsername(username));

const updatePassword = (dispatch: Dispatch<LoginPage.Message>) =>
  (password: string) =>
    dispatch(new UpdatePassword(password));

const login = (onSuccess: (profile: User.Profile) => void, dispatch: Dispatch<LoginPage.Message>) =>
  () =>
    dispatch(new Login(onSuccess));

/** Types */

interface Props {
  readonly state: LoginPage.State;
  readonly dispatch: Dispatch<LoginPage.Message>;

  readonly onRegister: () => void;
  readonly onSuccess: (profile: User.Profile) => void;
}

/** Styles */

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 16,
  },
  error: {
    marginBottom: 8,
    marginTop: 24,
    height: 20,
  },
  field: {
    marginBottom: 16,
  },
  form: {
    flex: 1,
    justifyContent: "center",
  },
  login: {
    marginBottom: 4,
  },
  register: {
    marginBottom: 4,
  },
});
