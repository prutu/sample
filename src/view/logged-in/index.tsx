import { Dispatch } from "futura";
import React from "react";
import { StyleSheet, View } from "react-native";

import { LoggedIn, Logout } from "app/state/logged-in";

import { ChatView } from "./chat";
import { HomeView } from "./home";
import { SettingsView } from "./settings";
import { TabsView } from "./tabs";

const TABS_HEIGHT = 50;

export const LoggedInView: React.SFC<Props> = ({ state, dispatch }) =>
  <View>
    <View style={styles.tabsContainer}>
      <TabsView
        loggedIn={state}
        dispatch={dispatch} />
    </View>
    <View style={styles.container}>
        {tab(state, dispatch)}
    </View>
  </View>;

const tab = (state: LoggedIn.State, dispatch: Dispatch<LoggedIn.Message>) => {
  switch (state.tab) {
    case LoggedIn.Tab.SettingsTab:
      return <SettingsView
                state={state.settings}
                dispatch={dispatch}
                onLogout={onLogout(state.instance, dispatch)} />;
    case LoggedIn.Tab.HomeTab:
      return <HomeView
                state={state.home}
                dispatch={dispatch} />;
    case LoggedIn.Tab.ChatTab:
      return <ChatView
                state={state.chat}
                dispatch={dispatch} />;
  }
};

/** Messages */

const onLogout = (instance: symbol, dispatch: Dispatch<LoggedIn.Message>) =>
  () =>
    dispatch(new Logout(instance));

/** Types */

interface Props {
  state: LoggedIn.State;
  dispatch: Dispatch<LoggedIn.Message>;
}

/** Styles */

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: TABS_HEIGHT,
  },
  tabsContainer: {
    height: TABS_HEIGHT,
  },
  tabs: {
  },
});
