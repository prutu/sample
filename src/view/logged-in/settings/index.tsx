import { Button } from "app/lib/view/component/form";
import { Dispatch } from "futura";
import React from "react";
import { StyleSheet, Text, View } from "react-native";

import {
  Logout,
  SettingsTab,
} from "app/state/logged-in/tabs/settings";


export const SettingsView: React.SFC<Props> = ({ state, onLogout, dispatch }) => {
  const loggingOut = state.status === SettingsTab.Status.LoggingOut;

  return (
    <View style={styles.container}>
      <Text>SettingsView</Text>
      <Button
        onPress={() => logout(onLogout, dispatch)}
        disabled={loggingOut}
        label={loggingOut ? "Logging Out" : "Log Out"} />
      {state.error
       ? <Text> {state.error} </Text>
       : null}
    </View>
  );
};

/** Messages */

const logout = (onLogout: () => void, dispatch: Dispatch<SettingsTab.Message>) =>
  dispatch(new Logout(onLogout));

/** Types */

interface Props {
  readonly state: SettingsTab.State;
  readonly dispatch: Dispatch<SettingsTab.Message>;

  readonly onLogout: () => void;
}

/** Styles */

const styles = StyleSheet.create({
  container: {
    alignItems: "center",
    flex: 1,
    justifyContent: "center",
  },
});
