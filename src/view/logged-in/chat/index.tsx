import { Dispatch } from "futura";
import React from "react";
import { StyleSheet, Text, View } from "react-native";

import { ChatTab } from "app/state/logged-in/tabs/chat";

export const ChatView: React.SFC<Props> = () =>
  <View style={styles.container}>
    <Text>ChatView</Text>
  </View>;

interface Props {
  readonly state: ChatTab.State;
  readonly dispatch: Dispatch<ChatTab.Message>;
}

/** Styles */

const styles = StyleSheet.create({
  container: {
    alignItems: "center",
    flex: 1,
    justifyContent: "center",
  },
});
