import { Dispatch } from "futura";
import React from "react";
import { StyleSheet, Text, View } from "react-native";

import { HomeTab } from "app/state/logged-in/tabs/home";

export const HomeView: React.SFC<Props> = ({ state }) => {
  return (
    <View style={styles.container}>
      <Text> HomeView </Text>
      <Text> {state.user.username} </Text>
    </View>
  );
};

interface Props {
  readonly state: HomeTab.State;
  readonly dispatch: Dispatch<HomeTab.Message>;
}

/** Styles */

const styles = StyleSheet.create({
  container: {
    alignItems: "center",
    flex: 1,
    justifyContent: "center",
  },
});
