import { Dispatch } from "futura";
import React, { Component } from "react";
import { StyleSheet, View } from "react-native";

import { messages as i18n } from "app/i18n";
import { chat, home, settings } from "app/lib/view/icon";

import { LoggedIn, SelectTab } from "app/state/logged-in";

import { TabView } from "./tab";


const _ = i18n.loggedIn.tabs;

export class TabsView extends Component<Props> {
  public shouldComponentUpdate(nextProps: Props) {
    return this.props.loggedIn.tab !== nextProps.loggedIn.tab
      || this.props.loggedIn.instance !== nextProps.loggedIn.instance;
  }

  public render() {
    const { loggedIn } = this.props;

    return (
      <View style={styles.container}>
        <TabView
          label={_.settings}
          icon={settings(loggedIn.tab === LoggedIn.Tab.SettingsTab)}
          selected={loggedIn.tab === LoggedIn.Tab.SettingsTab}
          onPress={this.selectSettings} />
        <TabView
          label={_.home}
          icon={home(loggedIn.tab === LoggedIn.Tab.HomeTab)}
          selected={loggedIn.tab === LoggedIn.Tab.HomeTab}
          onPress={this.selectHome} />
        <TabView
          label={_.chat}
          icon={chat(loggedIn.tab === LoggedIn.Tab.ChatTab)}
          selected={loggedIn.tab === LoggedIn.Tab.ChatTab}
          onPress={this.selectChat} />
      </View>
    );
  }

  private selectSettings = () => {
    this.selectTab(LoggedIn.Tab.SettingsTab);
  }

  private selectHome = () => {
    this.selectTab(LoggedIn.Tab.HomeTab);
  }

  private selectChat = () => {
    this.selectTab(LoggedIn.Tab.ChatTab);
  }

  private selectTab = (tab: LoggedIn.Tab) => {
    const { loggedIn, dispatch } = this.props;

    dispatch(new SelectTab(tab, loggedIn.instance));
  }
}


/** Types */

interface Props {
  loggedIn: LoggedIn.State;
  dispatch: Dispatch<LoggedIn.Message>;
}


/** Styles */

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "row",
    width: "100%",
    justifyContent: "space-evenly",
  },
});
