import React, { PureComponent } from "react";
import { Image, ImageURISource, StyleSheet, Text, TouchableWithoutFeedback, View } from "react-native";


export class TabView extends PureComponent<Props> {
  public render() {
    const { label, icon, selected, hasNotification = false, onPress } = this.props;

    return (
      <TouchableWithoutFeedback onPressIn={onPress}>
        <View style={styles.container}>
          {hasNotification
            ? <View style={styles.notification}></View>
            : null}
          <View style={selected ? [styles.content, styles.contentSelected] : styles.content}>
            <Image
              source={icon}
              resizeMode="contain" />
            <Text
                adjustsFontSizeToFit={true}
                numberOfLines={1}
                style={selected ? [styles.label] : styles.label}>
              {label}
            </Text>
          </View>
        </View>
      </TouchableWithoutFeedback>
    );
  }
}


/** Types */

export interface Props {
  label: string;
  icon: ImageURISource;
  selected: boolean;
  hasNotification?: boolean;
  onPress?: () => void;
}


/** Styles */

const styles = StyleSheet.create({
  container: {
    paddingTop: 10,
  },
  content: {
    paddingTop: 10,
    alignItems: "center",
    opacity: 0.33,
  },
  contentSelected: {
    opacity: 1,
  },
  label: {
    marginBottom: 1.5,
    textAlign: "center",
  },
  notification: {
    position: "absolute",
    top: 2, right: "50%", marginRight: -18,
    height: 8,
    width: 8,

    borderRadius: 8,
    shadowOpacity: 0.8,
    shadowOffset: { width: 0, height: 0 },
    shadowRadius: 2,
  },
});
