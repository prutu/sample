import { Dispatch } from "futura";
import React from "react";
import { Text } from "react-native";

import { Message, State } from "app/state";
import { Loading } from "app/state/loading";
import { LoggedIn } from "app/state/logged-in";
import { LoggedOut } from "app/state/logged-out";

import { LoadingView } from "./loading";
import { LoggedInView } from "./logged-in";
import { LoggedOutView } from "./logged-out";


export const AppView: React.SFC<Props> = ({ state, dispatch }) => {
  if (state instanceof Loading) {
    return <LoadingView />;
  } else if (state instanceof LoggedOut) {
    return <LoggedOutView
              state={state}
              dispatch={dispatch} />;
  } else if (state instanceof LoggedIn) {
    return <LoggedInView
              state={state}
              dispatch={dispatch} />;
  } else {
    return <Text>Not implemented</Text>;
  }
};

interface Props {
  state: State;
  dispatch: Dispatch<Message>;
}
