import { Dispatch, Service } from "futura";


export class Time implements Service<TimeReq, TimeSub> {
  private readonly dispatch: Dispatch<any>;

  constructor(dispatch: Dispatch<any>) {
    this.dispatch = dispatch;
  }

  public handleRequest(cmd: TimeReq) {
    switch (cmd.type) {
      case "after":
        setTimeout(() => this.dispatch(new cmd.Message(...cmd.params)), cmd.time);
    }
  }

  public updateSubscriptions(subs: ReadonlyArray<TimeSub>) {
    return;
  }
}

type TimeReq = AfterReq;
type TimeSub = never;

export interface AfterReq {
  type: "after";
  time: number;
  Message: AfterReqMessage<any>;
  params: any[];
}

export type AfterReqMessage<Message> = new(...params: any[]) => Message;
