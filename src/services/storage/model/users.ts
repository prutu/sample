/** Users */

export type Users = ReadonlyArray<Users.User>;

export namespace Users {
  export interface User {
    readonly email: string;
  }
}

export type User = Users.User;

export namespace User {
  export type ID = string;

  export interface Profile {
    readonly username: string;
  }
}
