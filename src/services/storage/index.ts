import { Req } from "futura";

import { User } from "./model/users";
import { Storage } from "./service";

export const register = <R, E, C>(
    username: string,
    Result: Result<string, R>,
    Error: Error<E>,
    context: C,
  ): Req<any> => ({
      service: Storage,
      request: {
        run: async () => {
          try {
            return new Result(username, context);
          } catch (e) {
            return new Error(ErrorInfo.make(e), context);
          }
        },
      },
  });

export const signIn = <R, E, C>(
  username: string,
  Result: Result<User.Profile, R>,
  Error: Error<E>,
  context: C,
): Req<any> => ({
    service: Storage,
    request: {
      run: async () => {
        try {
          if (username === "") {
            return new Error(ErrorInfo.make({
              message: "Username cannot be empty",
            }), context);
          }
          return new Result({ username }, context);
        } catch (e) {
          return new Error(ErrorInfo.make(e), context);
        }
      },
    },
});

export const signOut = <R, C, E>(
  Result: Result<boolean, R>,
  Error: Error<E>,
  context: C,
): Req<any> => ({
    service: Storage,
    request: {
      run: async () => {
        try {
          return new Result(true, context);
        } catch (e) {
          return new Error(ErrorInfo.make(e), context);
        }
      },
    },
});

type Result<T = any, R = any, C = any> = new(result: T, context: C) => R;

type Error<R = any, C = any> = Result<ErrorInfo, R, C>;

export interface ErrorInfo {
  code: number;
  message: string;
  data: any;
}

export namespace ErrorInfo {
  export const make = (info: Partial<ErrorInfo>) => ({
    code: info.code || 0,
    message: info.message || "Oops, something went wrong",
    data: info.data || {},
  });
}

export { User };
