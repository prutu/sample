import { Dispatch, Service } from "futura";

export class Storage implements Service<Req, Sub> {
  private readonly dispatch: Dispatch<any>;

  constructor(dispatch: Dispatch<any>) {
    this.dispatch = dispatch;
  }

  public async handleRequest(req: Req) {
    const result = await req.run();
    if (result !== undefined) {
      this.dispatch(result);
    }
  }

  public updateSubscriptions(subs: ReadonlyArray<Sub>) {
    return;
  }
}

export type Req
  = StorageReq.Authentication;

export namespace StorageReq {
  export interface Authentication {
    run(): Promise<any>;
  }
}

export type Sub = never;
