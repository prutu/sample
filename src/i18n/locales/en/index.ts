export const messages = {
  loggedIn: {
    tabs: {
      settings: "Settings",
      home: "Home",
      chat: "Chat",
    },
    settings: {
      title: "Settings",
      tabs: {
        albums: "Albums",
        artists: "Artists",
        playlists: "Playlists",
        stations: "Stations",
      },
    },
    home: {
      title: "Home",
    },
    chat: {
      title: "Chat",
      tabs: {
        albums: "Albums",
        artists: "Artists",
        playlists: "Playlists",
        stations: "Stations",
      },
    },
    /** Rooms */
    rooms: {
      rooms: "Rooms",
    },
  },
  /** Pages */
  page: {
    browse: {
      title: "Browse",
    },
    library: {
      title: "Library",

    },
    chat: {
      title: "Chat",
      placeholder: "Chat",
      tabs: {
        albums: "Albums",
        artists: "Artists",
        playlists: "Playlists",
        stations: "Stations",
      }
    },
  },
};
