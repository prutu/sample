
export const color = (value: ColorValue): Color => {
  if (typeof value === "string") {
    return parse(value);
  } else if ("r" in value) {
    return new RGBA(value.r, value.g, value.b, value.a);
  } else if ("h" in value) {
    return new HSLA(value.h, value.s, value.l, value.a);
  }

  throw new Error(`Invalid color value: ${value}`);
};

const parse = (value: string): Color => {
  const hex = parseHex(value);
  if (hex) {
    return hex;
  }

  throw new Error(`Invalid color value: ${value}`);
};

const parseHex = (value: string): Color | void => {
  if (/^#[a-f0-9]{3,4}$/i.test(value)) {
    const [, red, green, blue, alpha = "f"] = value;
    return new RGBA(
      parseInt(red + red, 16),
      parseInt(green + green, 16),
      parseInt(blue + blue, 16),
      parseInt(alpha + alpha, 16) / 255,
    );
  }

  if (/^#([a-f0-9]{6}|[a-f0-9]{8})$/i.test(value)) {
    const red = value.slice(1, 3);
    const green = value.slice(3, 5);
    const blue = value.slice(5, 7);
    const alpha = value.length === 7
      ? "ff"
      : value.slice(7, 9);
    return new RGBA(
      parseInt(red, 16),
      parseInt(green, 16),
      parseInt(blue, 16),
      parseInt(alpha, 16) / 255,
    );
  }
};


/** Types */

export interface Color {
  lighten(percent: number, relative?: boolean): Color;
  darken(percent: number, relative?: boolean): Color;
  saturate(percent: number, relative?: boolean): Color;
  desaturate(percent: number, relative?: boolean): Color;
  fade(percent: number): Color;

  toString(): string;
}

type ColorValue
  = { r: number, g: number, b: number, a?: number }
  | { h: number, s: number, l: number, a?: number }
  | string;

/** Internals */

class RGBA implements Color {
  private readonly red: number;
  private readonly green: number;
  private readonly blue: number;
  private readonly alpha: number;

  constructor(red: number, green: number, blue: number, alpha: number = 1.0) {
    this.red   = clamp(0, 255, red);
    this.green = clamp(0, 255, green);
    this.blue  = clamp(0, 255, blue);
    this.alpha = clamp(0, 1, alpha);
  }

  public lighten(percent: number, relative?: boolean) {
    return this.toHSLA().lighten(percent, relative);
  }

  public darken(percent: number, relative?: boolean) {
    return this.toHSLA().darken(percent, relative);
  }

  public saturate(percent: number, relative?: boolean) {
    return this.toHSLA().saturate(percent, relative);
  }

  public desaturate(percent: number, relative?: boolean) {
    return this.toHSLA().desaturate(percent, relative);
  }

  public fade(percent: number) {
    return new RGBA(this.red, this.green, this.blue, percent);
  }

  public toString() {
    const red = Math.round(this.red);
    const green = Math.round(this.green);
    const blue = Math.round(this.blue);

    return this.alpha === 1.0
      ? `rgb(${red}, ${green}, ${blue})`
      : `rgba(${red}, ${green}, ${blue}, ${this.alpha.toFixed(2)})`;
  }

  public toHSLA(): HSLA {
    const red = this.red / 255;
    const green = this.green / 255;
    const blue = this.blue / 255;
    const min = Math.min(red, green, blue);
    const max = Math.max(red, green, blue);
    const lightness = (min + max) / 2;
    const delta = max - min;

    let hue: number;
    if (max === min) {
      hue = 0;
    } else if (red === max) {
      hue = (green - blue) / delta;
    } else if (green === max) {
      hue = 2 + (blue - red) / delta;
    } else if (blue === max) {
      hue = 4 + (red - green) / delta;
    } else {
      hue = 0;
    }

    hue = Math.min(hue * 60, 360);

    if (hue < 0) {
      hue += 360;
    }

    let saturation: number;
    if (max === min) {
      saturation = 0;
    } else if (lightness <= 0.5) {
      saturation = delta / (max + min);
    } else {
      saturation = delta / (2 - max - min);
    }

    return new HSLA(hue, saturation, lightness, this.alpha);
  }
}

class HSLA implements Color {
  private readonly hue: number;
  private readonly saturation: number;
  private readonly lightness: number;
  private readonly alpha: number;

  constructor(hue: number, saturation: number, lightness: number, alpha: number = 1.0) {
    this.hue        = clamp(0, 360, hue);
    this.saturation = clamp(0, 1, saturation);
    this.lightness  = clamp(0, 1, lightness);
    this.alpha      = clamp(0, 1, alpha);
  }

  public lighten(percent: number, relative?: boolean) {
    const max = 1.0;
    const lightness = this.lightness + ((relative ? (max - this.lightness) : max) * percent);
    return new HSLA(this.hue, this.saturation, lightness, this.alpha);
  }

  public darken(percent: number, relative?: boolean) {
    const max = 1.0;
    const lightness = this.lightness - ((relative ? this.lightness : max) * percent);
    return new HSLA(this.hue, this.saturation, lightness, this.alpha);
  }

  public saturate(percent: number, relative?: boolean) {
    const max = 1.0;
    const saturation = this.saturation + ((relative ? (max - this.saturation) : max) * percent);
    return new HSLA(this.hue, saturation, this.lightness, this.alpha);
  }

  public desaturate(percent: number, relative?: boolean) {
    const max = 1.0;
    const saturation = this.saturation - ((relative ? this.saturation : max) * percent);
    return new HSLA(this.hue, saturation, this.lightness, this.alpha);
  }

  public fade(percent: number) {
    return new RGBA(this.hue, this.saturation, this.lightness, percent);
  }

  public toString() {
    const hue = Math.round(this.hue);
    const saturation = percent(this.saturation);
    const lightness = percent(this.lightness);

    return this.alpha === 1.0
      ? `hsl(${hue}, ${saturation}, ${lightness})`
      : `hsla(${hue}, ${saturation}, ${lightness}, ${this.alpha.toFixed(2)})`;
  }

  public toRGBA(): RGBA {
    const hue = this.hue / 360;
    const saturation = this.saturation;
    const lightness = this.lightness;

    if (saturation === 0) {
      const value = lightness * 255;
      return new RGBA(value, value, value, this.alpha);
    }

    const t2 = lightness < .5
      ? lightness * (1 + saturation)
      : lightness + saturation - lightness * saturation;
    const t1 = 2 * lightness - t2;

    let red = 0;
    let green = 0;
    let blue = 0;
    for (let i = 0; i < 3; i++) {
      let t3 = hue + 1 / 3 * -(i - 1);
      if (t3 < 0) {
        t3++;
      }
      if (t3 > 1) {
        t3--;
      }

      let value: number;
      if (6 * t3 < 1) {
        value = t1 + (t2 - t1) * 6 * t3;
      } else if (2 * t3 < 1) {
        value = t2;
      } else if (3 * t3 < 2) {
        value = t1 + (t2 - t1) * (2 / 3 - t3) * 6;
      } else {
        value = t1;
      }
      value *= 255;

      if (i === 0) {
        red = value;
      } else if (i === 1) {
        green = value;
      } else {
        blue = value;
      }
    }

    return new RGBA(red, green, blue, this.alpha);
  }
}

const clamp = (min: number, max: number, value: number) =>
  value < min
    ? min
    : value > max ? max : value;

const percent = (value: number) =>
  `${Math.round(value * 100)}%`;
