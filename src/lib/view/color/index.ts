import { color } from "./color";

export const black = color("#000000");
export const white = color("#ffffff");

export const ashGrey = color("#a9bab9");
export const darkJungleGreen = color("#1a2024");
export const desertSand = color("#e5d1aa");
export const gainsboro = color("#d3dadf");
export const gamboge = color("#e59e15");

export const oriolesOrange = color("#e55015");
export const lightGray = color("#ccc7ca");
export const peach = color("#ffdab9");
export const peacockBlue = color("#326872");
export const indigo = color("#4b0082");
export const bloodRed = color("#ff1f1f");
