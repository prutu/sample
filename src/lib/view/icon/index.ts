export const home = (selected: boolean) =>
  selected
  ? require("./icon-home-current.png")
  : require("./icon-home.png");

export const chat = (selected: boolean) =>
selected
? require("./icon-chat-current.png")
: require("./icon-chat.png");

export const settings = (selected: boolean) =>
selected
? require("./icon-settings-current.png")
: require("./icon-settings.png");

export enum Theme {
  Light = "light",
  Dark = "dark",
}

export const arrowDown = (selected: boolean) =>
  selected
    ? require("./icon-arrow-down-selected.png")
    : require("./icon-arrow-down.png");

export namespace chevron {
  export enum Theme {
    Light = "light",
    Dark = "dark",
  }

  export const down = (theme: Theme) =>
    theme === Theme.Light
      ? require("./icon-chevron-down-light.png")
      : require("./icon-chevron-down-dark.png");

  export const left = (theme: Theme) =>
    theme === Theme.Light
      ? require("./icon-chevron-left-light.png")
      : require("./icon-chevron-left-dark.png");

  export const right = (theme: Theme) =>
    theme === Theme.Light
      ? require("./icon-chevron-right-light.png")
      : require("./icon-chevron-right-dark.png");

  export const up = (theme: Theme) =>
    theme === Theme.Light
      ? require("./icon-chevron-up-light.png")
      : require("./icon-chevron-up-dark.png");
}

export namespace arrow {
  export enum Theme {
    Light = "light",
    Dark = "dark",
  }

  export const left = (theme: Theme) =>
    theme === Theme.Light
      ? require("./icon-arrow-left-light.png")
      : require("./icon-arrow-left-dark.png");
}

export namespace ImageIcons {
  export const gallery = () =>
    require("./icon-gallery.png");

  export const edit = () =>
    require("./icon-edit.png");

  export const remove = () =>
    require("./icon-trashcan.png");
}

export namespace wifi {
  export const strong = () =>
    require("./icon-wifi-strong.png");

  export const medium = () =>
    require("./icon-wifi-medium.png");

  export const weak = () =>
    require("./icon-wifi-weak.png");

  export const secure = () =>
    require("./icon-wifi-secure.png");
}

export const checkmark = () =>
  require("./icon-checkmark.png");
