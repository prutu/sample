import React from "react";
import { StyleProp, StyleSheet, Text, View, ViewStyle } from "react-native";

import * as typography from "app/lib/view/typography";


export const Field: React.SFC<Props> = ({ label, style, children }) =>
  <View style={style}>
    {label
      ? <Text style={styles.label}>{label}</Text>
      : null}
    { children }
  </View>;


/** Types */

interface Props {
  label?: string;
  style?: StyleProp<ViewStyle>;
}


/** Style */

const styles = StyleSheet.create({
  label: {
    marginBottom: 4,

    ...typography.dark.subhead,
    opacity: 0.5,
  },
});
