import React from "react";
import { StyleSheet, TextInput as BaseTextInput } from "react-native";

import { darkJungleGreen } from "app/lib/view/color";


export class TextInput extends BaseTextInput {
  public render() {
    const { style, ...props } = this.props;

    return <BaseTextInput
      underlineColorAndroid="transparent"
      style={[styles.textInput, style]}
      {...props} />;
  }
}


/** Style */

const styles = StyleSheet.create({
  textInput: {
    paddingBottom: 12,
    paddingLeft: 8,
    paddingRight: 8,
    paddingTop: 12,

    backgroundColor: darkJungleGreen.desaturate(1).lighten(0.9).toString(),
    borderColor: darkJungleGreen.desaturate(0.8).lighten(0.8).toString(),
    borderRadius: 2,
    borderWidth: 1,
  },
});
