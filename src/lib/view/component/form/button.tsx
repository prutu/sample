import React from "react";
import { StyleSheet, Text, TouchableOpacity, TouchableOpacityProperties } from "react-native";

import { darkJungleGreen, gamboge, white } from "app/lib/view/color";
import * as typography from "app/lib/view/typography";


export const Button: React.SFC<Props> = ({ label, disabled, primary = true, style, ...props }) =>
  <TouchableOpacity
      activeOpacity={0.6}
      disabled={disabled}
      style={!disabled
        ? [ styles.container, primary && styles.containerPrimary, style ]
        : [ styles.container, styles.containerDisabled, style ]}
      {...props}>
    <Text style={styles.label}>{label}</Text>
  </TouchableOpacity>;


/** Types */

interface Props extends TouchableOpacityProperties {
  label: string;
  primary?: boolean;
}


/** Styles */

const styles = StyleSheet.create({
  container: {
    padding: 12,

    backgroundColor: darkJungleGreen.toString(),
    borderRadius: 4,
  },
  containerDisabled: {
    backgroundColor: gamboge.desaturate(1).lighten(0.4).toString(),
  },
  containerPrimary: {
    backgroundColor: gamboge.toString(),
  },
  label: {
    ...typography.light.callout,
    ...typography.weight.bold,
    color: white.toString(),
    textAlign: "center",
  },
});
