import React, { Component, ReactChild, ReactNode } from "react";
import { Animated, BackHandler, Platform, StyleSheet, View } from "react-native";

import { Card } from "./card";


export class StackView extends Component<Props, State> {
  constructor(props: Props & { children?: ReactNode }) {
    super(props);

    const children = React.Children.toArray(props.children);
    this.state = {
      children: children.map((child) => ({
        child,
        transition: new Animated.Value(1),
        removing: false,
      })),
      top: children.length - 1,
      width: 0,
      height: 0,
    };
  }

  public static getDerivedStateFromProps(
      nextProps: Props & { children?: ReactNode },
      currentState: State,
  ): Partial<State> {
    const next = React.Children.toArray(nextProps.children);
    const current = currentState.children;

    if (next.length > current.length) {
      const children = next.map((child, index) =>
        index < current.length
          ? { child, transition: current[index].transition, removing: false }
          : { child, transition: new Animated.Value(0), removing: false });
      return {
        children,
        top: next.length - 1,
      };
    } else {
      const children = current.map(({ child, transition }, index) =>
        index < next.length
          ? { child: next[index], transition, removing: false }
          : { child, transition, removing: true });
      return {
        children,
        top: next.length - 1,
      };
    }
  }

  public componentDidUpdate(prevProps: Props & { children?: ReactNode }, prevState: State) {
    const { children } = this.state;
    const prev = prevState.children;

    children.forEach(({ child, transition, removing }, index) => {
      if (index < prev.length) {
        // Unchanged and Removed
        if (removing) {
          Transitions.remove(transition).start(({ finished }) => {
            if (finished) {
              const children = this.state.children.filter(({ child: c }) => c !== child);
              this.setState({ children });
            }
          });
        } else if (prev[index].removing) {
          // Cancel removal
          transition.stopAnimation();
          Transitions.add(transition).start();
        }
      } else {
        // Added
        Transitions.add(transition).start();
      }
    });

  }

  public componentDidMount() {
    BackHandler.addEventListener("hardwareBackPress", this.onBack);
  }

  public componentWillUnmount() {
    BackHandler.removeEventListener("hardwareBackPress", this.onBack);
  }

  public render() {
    const { children, top, width } = this.state;

    return (
      <View
          pointerEvents="box-none"
          onLayout={this.onLayout}
          style={styles.container}>
        { children.map(({ child, transition }, index) => {
            const style = {
              opacity: transition.interpolate({
                inputRange: [0, 1],
                outputRange: [0, 1],
              }),
              transform: [
                Platform.select({
                  android: {
                    translateY: transition.interpolate({
                      inputRange: [0, 1],
                      outputRange: [40, 0],
                    }),
                  },
                  default: {
                    translateX: transition.interpolate({
                      inputRange: [0, 1],
                      outputRange: [width, 0],
                    }),
                  },
                }),
              ],
            };

            return (
              <Card
                  key={index.toString()}
                  pointerEvents={index === top ? "auto" : "none"}
                  style={style}>
                {child}
              </Card>
            );
          }) }
      </View>
    );
  }

  private onLayout = (event: any) => {
    const { height, width } = event.nativeEvent.layout;

    if (height !== this.state.height || width !== this.state.width) {
      this.setState({
        height,
        width,
      });
    }
  }

  private onBack = () => {
    return this.props.onBack();
  }
}

namespace Transitions {
  export const add = (value: Animated.Value) =>
    Animated.spring(value, {
      toValue: 1,
      stiffness: 1000,
      damping: 500,
      mass: 3,
      useNativeDriver: true,
    });

  export const remove = (value: Animated.Value) =>
    Animated.spring(value, {
      toValue: 0,
      stiffness: 1000,
      damping: 500,
      mass: 3,
      useNativeDriver: true,
    });
}


/** Types */

interface Props {
  onBack: () => boolean;
}

interface State {
  children: ReadonlyArray<Child>;
  top: number; /** Index of the top element */
  width: number;
  height: number;
}

interface Child {
  readonly child: ReactChild;
  readonly transition: Animated.Value;
  readonly removing: boolean;
}


/** Styles */

const styles = StyleSheet.create({
  container: {
    flex: 1,
    overflow: "hidden",
  },
});
