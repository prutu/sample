import React from "react";
import { Animated, StyleSheet } from "react-native";

import { white } from "app/lib/view/color";


export const Card: React.SFC<Props> = ({ children, pointerEvents, style }) =>
  <Animated.View
      style={[ styles.container, style ]}
      pointerEvents={pointerEvents} >
    {children}
  </Animated.View>;


/** Types */

interface Props {
  pointerEvents?: "box-none" | "none" | "box-only" | "auto";
  style?: any;
}

/** Styles */

const styles = StyleSheet.create({
  container: {
    position: "absolute",
    top: 0, right: 0, bottom: 0, left: 0,

    backgroundColor: white.toString(),
  },
});
