import React from "react";
import { ActivityIndicator, StyleSheet, View } from "react-native";


export const LoadingView = () =>
  <View style={styles.container}>
    <ActivityIndicator />
  </View>;


/** Styles */

const styles = StyleSheet.create({
  container: {
    flex: 1,

    alignItems: "center",
    justifyContent: "center",
  },
});
