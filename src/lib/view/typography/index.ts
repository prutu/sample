import { human, systemWeights } from "react-native-typography";


export namespace dark {
  export const largeTitle = human.largeTitleObject;
  export const title1 = human.title1Object;
  export const title2 = human.title2Object;
  export const title3 = human.title3Object;
  export const headline = human.headlineObject;
  export const body = human.bodyObject;
  export const callout = human.calloutObject;
  export const subhead = human.subheadObject;
  export const footnote = human.footnoteObject;
  export const caption1 = human.caption1Object;
  export const caption2 = human.caption2Object;
}

export namespace light {
  export const largeTitle = human.largeTitleWhiteObject;
  export const title1 = human.title1WhiteObject;
  export const title2 = human.title2WhiteObject;
  export const title3 = human.title3WhiteObject;
  export const headline = human.headlineWhiteObject;
  export const body = human.bodyWhiteObject;
  export const callout = human.calloutWhiteObject;
  export const subhead = human.subheadWhiteObject;
  export const footnote = human.footnoteWhiteObject;
  export const caption1 = human.caption1WhiteObject;
  export const caption2 = human.caption2WhiteObject;
}

export namespace weight {
  export const thin = systemWeights.thin;
  export const light = systemWeights.light;
  export const regular = systemWeights.regular;
  export const semibold = systemWeights.semibold;
  export const bold = systemWeights.bold;
}
