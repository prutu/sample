import { program, Program } from "futura";
import React, { Component } from "react";

import { StatusBar, StyleSheet, View } from "react-native";

import { init, Message, State, subscriptions, update } from "app/state";
import { AppView } from "app/view";

export default class App extends Component<AppProps, AppState> {
  private app: Program<State, Message>;
  private subscription?: any;
  private pendingState?: State;

  constructor(props: AppProps) {
    super(props);

    this.app = program<State, Message>({ init, update, subscriptions });
    this.state = {
      state: this.app.state,
    };
  }

  public shouldComponentUpdate(nextProps: Readonly<AppProps>, nextState: Readonly<AppState>) {
    return nextState.state !== this.state.state;
  }

  public componentDidMount() {
    this.subscription = this.app.observe((state) => {
      if (this.pendingState === undefined) {
        requestAnimationFrame(() => {
          const state = this.pendingState;
          this.pendingState = undefined;
          if (state !== undefined) {
            this.setState({ state });
          }
        });
      }
      this.pendingState = state;
    });
  }

  public componentWillUnmount() {
    this.subscription!.cancel();
    this.subscription = undefined;
    this.pendingState = undefined;
  }

  public render() {
    const { state } = this.state;

    return (
      <View style={styles.container}>
        <StatusBar
          animated={true}
          backgroundColor="transparent"
          barStyle="dark-content"
          hidden={true}
          showHideTransition="fade"
          translucent={true} />
        <AppView
          state={state}
          dispatch={this.dispatch} />
      </View>
    );
  }

  private dispatch = (message: Message) => {
    requestAnimationFrame(() => {
      this.app.update(message);
    });
  }
}

/** Types */

interface AppProps {
}

interface AppState {
  readonly state: State;
}

/** Styles */

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#F5FCFF",
  },
});
